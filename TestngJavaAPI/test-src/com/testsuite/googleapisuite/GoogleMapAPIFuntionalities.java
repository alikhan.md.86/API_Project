/**************************************** PURPOSE **********************************

 This class contains the automated API tests with different ways. Please feel free to use any one of the ways to automate based on requirement -
	 - Hard coded test with minimal abstraction at API layer for better readability
	 - Remaining tests with data abstraction i.e. reading data from Properties file
	 - Data driven test using Data provider i.e reading from Excel 
	 - Test which is intentionally failed to show in reporting
 
 */
package com.testsuite.googleapisuite;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.ParseException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.base.BaseSetup;
import com.datamanager.ExcelManager;
import com.utilities.APIHelpers;

public class GoogleMapAPIFuntionalities extends BaseSetup {

	// Declaration of respective API Parts instances
	ExcelManager excel_Manage;
	private APIHelpers apiHelpers;
	public static String current_TestName;
	public int test_Run_count = 0;
	private int EXCEL_ROWNUMBER = 2;
	private int EXCEL_ROW_INDEX = 1;
	List<String> headers = new ArrayList<String>();
	HashMap<String, String> keys_Values = new HashMap<>();
	

	/**
	 * Purpose - Initializes the Page parts instances
	 */
	@BeforeMethod(alwaysRun = true)
	public void SetUp(Method method) {
		apiHelpers = new APIHelpers();
		current_TestName = method.getName();
		headers.add(API_Data.header_content_Type);
		keys_Values.put("place_id_key", "place_id");
		keys_Values.put("address_key", "formatted_address");
		keys_Values.put("place_id_value", "ChIJ7cv00DwsDogRAMDACa2m4K8");
		keys_Values.put("address_value", "Chicago, IL, USA");
		keys_Values.put("place_id_Fail", "ChIH4dJ7cv00DwsDurgamRAMDACa2m4K8");
	}

	/**
	 * Purpose - Performs a Get request, get Json response
	 * (http://maps.googleapis
	 * .com/maps/api/geocode/json?address=chicago&sensor=false) and verifies the
	 * response message This test script is created with hard coded test data
	 * for better readability
	 */
	@Test(groups = { "smoke" }, timeOut = 5000)
	public void tc_API_006_GetPlaceIDAdressAndVerifyUsingGetMethod() {

		// Read request parameters data from excel file
		String req_KeyValue_Params = apiHelpers.readRequestQueryParamsFromExcel(API_Data.excel_File,API_Data.excel_SheetName, EXCEL_ROWNUMBER);
		// 2. Adding API headers
		// 3. Execute API request -
		// 4. Passing arguments:
		// a. API end point
		// b. 'true' value for appending query parameters (if query parameters
		// not required set to 'false')

		HashMap<String, String> response = apiHelpers.executeRequest(null,
				null, API_Data.googleMap_endpoint, headers, "GET", true,req_KeyValue_Params);
		// 5. Verify response code (200) and message (OK)
		apiHelpers.verifyStatusCode(response.get("status code"), "200");
		apiHelpers.verifyStatusMessage(response.get("status message"), "OK");
		// 6. verifying values for respective keys in the response body
		// a. response body (String type)
		// b.Root/Parent node
		// c. keys to get actual values and expected values to be verified with
		// actual
		apiHelpers.verifyValuesforkeys(response.get("response body"),"results", keys_Values);
        // 7. Getting and Verifying response headers	
		HashMap<String, String> response_Headers=apiHelpers.getResponseHeaders();
		Assert.assertEquals(response_Headers.get("Content-Type"), "application/json; charset=UTF-8");
		
	}

	/**
	 * Purpose - Performs a Get request, get Json response
	 * (http://maps.googleapis
	 * .com/maps/api/geocode/json?address=chicago&sensor=false) and verifies the
	 * response message
	 */
	@Test(dataProvider = "Parameters", groups = { "regression" }, timeOut = 500000)
	public void tc_API_001_GetPlaceIDAdressAndVerifyUsingGetMethod(
			String address_Val, String sensor_Val, String name_Val) {

		// Read keys data from excel file
		excel_Manage = new ExcelManager(API_Data.excel_File);
		List<String> key_Params = excel_Manage.getExcelRowData(
				API_Data.excel_DataProvider_SheetName, EXCEL_ROW_INDEX);
		// execute API request
		HashMap<String, String> response = apiHelpers.executeGetRequestDataProvider(null, null,
						API_Data.googleMap_endpoint, headers,
						true, key_Params.size(), key_Params, address_Val,
						sensor_Val, name_Val);
		// Verify response message and body
		apiHelpers.verifyStatusCode(response.get("status code"), "200");
		apiHelpers.verifyStatusMessage(response.get("status message"), "OK");
		
		String[] place_id_Key = {"place_id"};
		String[] place_Id_VAluesValues = {"ChIJ7cv00DwsDogRAMDACa2m4K8","ChIJCzYy5IS16lQRQrfeQ5K5Oxw","ChIJHRv42bxQa4gRcuwyy84vEH4"};
		apiHelpers.verifyValuesforkeys(response.get("response body"),
				API_Data.root_Node, test_Run_count, place_id_Key, place_Id_VAluesValues);
		test_Run_count++;

	}

	/**
	 * Purpose - Performs a Get request, get Json response
	 * (http://maps.googleapis
	 * .com/maps/api/geocode/json?address=chicago&sensor=false) and verifies the
	 * response message
	 * 
	 * @throws IOException
	 * @throws ParseException
	 */
	@Test(groups = { "regression" } , timeOut=5000)
	public void tc_API_002_GetPlaceIDAdressAndVerifyUsingGetMethod() {

		// Read request parameters data from excel file
		String req_KeyValue_Params = apiHelpers
				.readRequestQueryParamsFromExcel(API_Data.excel_File,
						API_Data.excel_SheetName, EXCEL_ROWNUMBER);
		// execute API request
		HashMap<String, String> response = apiHelpers.executeRequest(null,
				null, API_Data.googleMap_endpoint, headers, "GET", true,
				req_KeyValue_Params,
				API_Data.request_Body_File, API_Data.reqBody_Param);
		// //Verify response message and body
		apiHelpers.verifyStatusCode(response.get("status code"), "200");
		apiHelpers.verifyStatusMessage(response.get("status message"), "OK");
		apiHelpers.verifyValuesforkeys(response.get("response body"),
				"results", keys_Values);
	}

	/**
	 * Purpose - Performs a Post request, get Json response
	 * (http://maps.googleapis
	 * .com/maps/api/geocode/json?address=chicago&sensor=false) and verifies the
	 * response message This test is intentionally failing for failure reports
	 */
	@Test(groups = { "regression" }, timeOut = 5000)
	public void tc_API_003_GetPlaceIDAdressAndVerifyUsingPostMethod()
			throws IOException {

		// Read request parameters data from excel file
		String req_KeyValue_Params = apiHelpers
				.readRequestQueryParamsFromExcel(API_Data.excel_File,
						API_Data.excel_SheetName, EXCEL_ROWNUMBER);
		// execute API request
		HashMap<String, String> response = apiHelpers.executeRequest(null,
				null, API_Data.googleMap_endpoint, headers, "POST", true,
				req_KeyValue_Params,
				API_Data.request_Body_File, API_Data.root_Node,
				API_Data.formatted_Address_Key, API_Data.reqBody_Param);
		// Verify response message and body
		apiHelpers.verifyStatusCode(response.get("status code"), "200");
		apiHelpers.verifyStatusMessage(response.get("status message"), "OK");
		apiHelpers.verifyValuesforkeys(response.get("response body"),
				"results",keys_Values.get("place_id_key"),keys_Values.get("place_id_Fail"));
	}

	/**
	 * Purpose - Performs request body, Post request, get Json response
	 * (http://maps
	 * .googleapis.com/maps/api/geocode/json?address=chicago&sensor=false) and
	 * verifies the response message
	 */
	@Test(groups = { "regression" }, timeOut = 5000)
	public void tc_API_004_UpdatePlaceIDAdressAndVerifyUsingPostMethod() {

		// Read request parameters data from excel file
		String req_KeyValue_Params = apiHelpers
				.readRequestQueryParamsFromExcel(API_Data.excel_File,
						API_Data.excel_SheetName, EXCEL_ROWNUMBER);
		// execute API
		HashMap<String, String> response = apiHelpers.executeRequest(null,
				null, API_Data.googleMap_endpoint, headers, "POST", true,
				req_KeyValue_Params,
				API_Data.request_Body_File, API_Data.root_Node,
				API_Data.formatted_Address_Key, API_Data.reqBody_Param);
		// Verify response message and body
		apiHelpers.verifyStatusCode(response.get("status code"), "200");
		apiHelpers.verifyStatusMessage(response.get("status message"), "OK");
		apiHelpers.verifyValuesforkeys(response.get("response body"),
				"results", keys_Values);
	}

	/**
	 * Purpose - Performs a Get request, get Json response
	 * (http://maps.googleapis
	 * .com/maps/api/geocode/json?address=chicago&sensor=false) and verifies the
	 * response message here we used regular expression methods to verify
	 * response
	 * 
	 */
	@Test(groups = { "regression" }, timeOut = 5000)
	public void tc_API_005_verifyPlaceAddressUsingRegularExpressions() {

		// Read request parameters data from excel file
		String req_KeyValue_Params = apiHelpers
				.readRequestQueryParamsFromExcel(API_Data.excel_File,
						API_Data.excel_SheetName, EXCEL_ROWNUMBER);
		// execute API request
		HashMap<String, String> response = apiHelpers.executeRequest(null,
				null, API_Data.googleMap_endpoint, headers, "GET", true,
				req_KeyValue_Params,
				API_Data.request_Body_File, API_Data.reqBody_Param);
		// Verify response message and body
		apiHelpers.verifyStatusCode(response.get("status code"), "200");
		apiHelpers.verifyStatusMessage(response.get("status message"), "OK");
		apiHelpers.verifyPatternExistsInResponse(response.get("response body"),API_Data.address_Regex);
		apiHelpers.verifyPatternCountToBeFromResponse(response.get("response body"), API_Data.address_Regex,API_Data.regex_Count);
		apiHelpers.verifyTextExistsInResponse(response.get("response body"),API_Data.address_Regex, API_Data.expected_Address);
	}
}