package com.apiTestData;

import com.datamanager.ConfigManager;

public class GoogleMapAPIData {
	
	private ConfigManager apiData=new ConfigManager("GoogleMapApi");
	public String excel_File=System.getProperty("user.dir")+apiData.getProperty("Api.Excel.Googl_API_Req_Params_File");
	public String request_Body_File=System.getProperty("user.dir")+apiData.getProperty("Api.request_Body_File");
	public String excel_SheetName=apiData.getProperty("Api.Excel.Req_Params_SheetName");
	public String excel_DataProvider_SheetName=apiData.getProperty("Api.Excel.Req_Params_DataProvider_SheetName");
	private final String google_map_base_URL = apiData.getProperty("Api.google_map_base_URL");
	public final String googleMap_endpoint = google_map_base_URL+apiData.getProperty("Api.googleMap_endpoint");
	public final String header_content_Type= apiData.getProperty("Api.Header.contentType");
	public final String  root_Node= apiData.getProperty("Api.root_Node");
	public final String  formatted_Address_Key= apiData.getProperty("Api.formatted_Address_Key");
	public final String  formatted_address_Key_Value= apiData.getProperty("Api.formatted_address_Key_Value");
	public final String place_id_Key = apiData.getProperty("Api.place_id_Key");
	public final String place_id_Value = apiData.getProperty("Api.place_id_Value");
	public final String Keys = apiData.getProperty("Api.Keys");
	public final String values = apiData.getProperty("Api.values");
	public final String  place_ids_Dataproviders= apiData.getProperty("Api.place_ids_Dataproviders");
	public final String reqBody_Param = apiData.getProperty("Api.reqBody_Param");
	public final String  formatted_address_Key_Value2= apiData.getProperty("Api.formatted_address_Key_Value2");
	public final String  address_Regex= apiData.getProperty("Api.address_Regex");
	public final String  expected_Address= apiData.getProperty("Api.expected_Address");
	public final int  regex_Count= Integer.parseInt(apiData.getProperty("Api.regex_Count"));
	public final String  place_id_Value_Fail= apiData.getProperty("Api.place_id_Value_Fail");
}