/**************************************** PURPOSE **********************************

 - This class contains the code related to Basic setup of TestSuites such as Create Results Folder, Adding steps LogFile

USAGE
 - Inherit this BaseClass for any TestSuite class. You don't have to write any @Beforeclass and @AfterClass
 - actions in your TestSuite Classes
 
 - Example: 
 --import Com.Base
 --- public class <TestSuiteClassName> extends BaseClass
 */
package com.base;

import java.io.File;

import org.apache.log4j.Logger;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;

import com.apiTestData.GoogleMapAPIData;
import com.datamanager.ConfigManager;
import com.datamanager.ExcelManager;
import com.datamanager.IExcelManager;
import com.utilities.ReportSetup;
import com.utilities.UtilityMethods;

@Listeners({ com.listener.TestListener.class })
public class BaseSetup{
	private boolean isReportFolderCreated = true;
	private Logger log = Logger.getLogger("BaseClass");
	private ConfigManager test = new ConfigManager("TestDependency");
	public IExcelManager excel;
	public GoogleMapAPIData	API_Data=new GoogleMapAPIData();
	
	/**
	 * Creates folder structure to store the automation reports
	 * 
	 * @throws Exception
	 */
	@BeforeSuite
	public void beforeSuite() throws Exception {
		if (isReportFolderCreated) {
			ReportSetup.createFolderStructure();
			isReportFolderCreated = false;
		}
		log.info("<h2>--------------------SuiteRunner Log-------------------------<h2>");
	}
	

	/**
	 * 
	 * This method adds Log file link to ReportNG report
	 * 
	 * @throws Exception
	 */
	@AfterSuite
	public void AddLogFileToReport() throws Exception {
		log.info("after suite");
		String sSeperator = UtilityMethods.getFileSeperator();
		String logFilePath = ".." + sSeperator + "Log.log";
		Reporter.log("<br>");
		Reporter.log("<a class=\"cbutton\" href=\"" + logFilePath
				+ "\">Click to Open Log File</a>");
		String PageLoadTimeSummaryFilePath = ".." + sSeperator
				+ "PageLoadTime_Summary.html";
		File f = new File(PageLoadTimeSummaryFilePath);
		if (f.exists()) {
			Reporter.log("<br>");
			Reporter.log("<a class=\"cbutton\" href=\""
					+ PageLoadTimeSummaryFilePath
					+ "\">Click to Open PageLoad Time Summary File</a>");
		}
	}


	/**
	 * 
	 * This method is used to know whether the dependent test has passed or not
	 * 
	 * @param dependentTestName
	 *            , Need to pass the dependent test name
	 * @throws SkipException
	 */
	public void hasDependentTestMethodPassed(String dependentTestName)
			throws SkipException {
		String currentTestName = Thread.currentThread().getStackTrace()[2]
				.getMethodName();
		if (test.getProperty(dependentTestName) != null) {
			if (test.getProperty(dependentTestName).equalsIgnoreCase("pass")) {
				log.info("dependent test - " + dependentTestName
						+ " has passed \n Running test - " + currentTestName);
			} else {
				log.info("dependent test - " + dependentTestName
						+ " has failed \n Hence test - " + currentTestName
						+ "is skipped");
				throw new SkipException("Dependent test - " + dependentTestName
						+ " has failed \n Hence test - " + currentTestName
						+ "is skipped");
			}
		} else {
			log.info("dependent test - " + dependentTestName
					+ " did not run \n Hence test - " + currentTestName
					+ "is skipped");
			throw new SkipException("Dependent test - " + dependentTestName
					+ " did not run \n Hence test - " + currentTestName
					+ "is skipped");
		}

	}
	/**
	 * 
	 * This method used for data provider to get data from excel sheet.
	 * 
	 * @throws Exception
	 */
	@DataProvider(name="Parameters")
	public Object[][] parametersData() {
		excel=new ExcelManager(API_Data.excel_File);
		Object[][] arrayObject =excel.getExcelSheetData(API_Data.excel_DataProvider_SheetName);
		return arrayObject;
	}
}
