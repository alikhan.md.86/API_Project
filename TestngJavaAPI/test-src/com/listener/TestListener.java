package com.listener;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

import com.datamanager.ConfigManager;
import com.testRail.TestRailResultUpdator;
import com.utilities.UtilityMethods;

public class TestListener extends TestListenerAdapter implements ISuiteListener
{

	ConfigManager sys = new ConfigManager();
	ConfigManager depend = new ConfigManager("TestDependency");
	Logger log =Logger.getLogger("TestListener");
	TestRailResultUpdator testRailResultUpdator = new TestRailResultUpdator();
	private static boolean milestoneCreated = false;

	/**
	 * This method will be called if a test case is failed. 
	 * Purpose - For attaching captured screenshots and videos in ReportNG report 
	 */
	public void onTestFailure(ITestResult result)
	{
		depend.writeProperty(result.getName(),"Fail");

		log.error("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" );
		log.error("ERROR ----------"+result.getName()+" has failed-----------------" );
		log.error("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n" );
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		Reporter.setCurrentTestResult(result);
		String id = getCaseId(result.getName());
		updateToTestRail(id, result.getName(), 5);
		UtilityMethods.verifyPopUp();
		Reporter.setCurrentTestResult(null);
	}

	/**
	 * This method will be called if a test case is skipped. 
	 * 
	 */
	public void onTestSkipped(ITestResult result)
	{			
		log.warn("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" );
		log.warn("WARN ------------"+result.getName()+" has skipped-----------------" );
		log.warn("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" );			
		depend.writeProperty(result.getName(),"Skip");

		//************* comment below code if you are using TestNG dependency methods
		String id = getCaseId(result.getName());
		updateToTestRail(id, result.getName(), 2);
		Reporter.setCurrentTestResult(result);
		UtilityMethods.verifyPopUp();
		Reporter.setCurrentTestResult(null);
	}

	/**
	 * This method will be called if a test case is passed. 
	 * Purpose - For attaching captured videos in ReportNG report 
	 */
	public void onTestSuccess(ITestResult result)
	{
		depend.writeProperty(result.getName(),"Pass");
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		log.info("###############################################################" );
		log.info("SUCCESS ---------"+result.getName()+" has passed-----------------" );
		log.info("###############################################################" );
		String id = getCaseId(result.getName());
		updateToTestRail(id, result.getName(), 1);
		Reporter.setCurrentTestResult(result);
		UtilityMethods.verifyPopUp();
		Reporter.setCurrentTestResult(null);
	}

	/**
	 * This method will be called before a test case is executed. 
	 * Purpose - For starting video capture and launching balloon popup in ReportNG report 
	 */
	public void onTestStart(ITestResult result)
	{
		log.infoLevel("<h2>**************CURRENTLY RUNNING TEST************ "+result.getName()+"</h2>" );
		UtilityMethods.currentRunningTestCaseBalloonPopUp(result.getName());
	}

	public void onStart(ITestContext context) 
	{

	}

	public void onFinish(ITestContext context) 
	{
		Iterator<ITestResult> failedTestCases = context.getFailedTests().getAllResults().iterator();
		while (failedTestCases.hasNext())
		{
			ITestResult failedTestCase = failedTestCases.next();
			ITestNGMethod method = failedTestCase.getMethod();            
			if (context.getFailedTests().getResults(method).size() > 1)
			{
				if(sys.getProperty("KeepFailedResult").equalsIgnoreCase("false")){
					//log.info("failed test case remove as dup:" + failedTestCase.getTestClass().toString());
					failedTestCases.remove(); 
				}
			}
			else
			{	                
				if (context.getPassedTests().getResults(method).size() > 0)
				{
					if(sys.getProperty("KeepFailedResult").equalsIgnoreCase("false")){
						//log.info("failed test case remove as pass retry:" + failedTestCase.getTestClass().toString());
						failedTestCases.remove();
					}	                    
				}                          
			}            
		}
	}


	@Override
	public void onStart(ISuite iSuite) {
		if(sys.getProperty("UpdateResultsToTestRail").equalsIgnoreCase("true")){
			if(!milestoneCreated){
				testRailResultUpdator.createMilestoneAndAddRuns();
				milestoneCreated = true;
			}
		}
	}



	/**
	 * This method is used to update the results to testrail
	 * @param id - test case id
	 * @param testName - name of the test method
	 * @param testStatus - test method status
	 */
	private void updateToTestRail(String id, String testName, int testStatus) {
		if (sys.getProperty("UpdateResultsToTestRail").equalsIgnoreCase("true")) {
			if(UtilityMethods.isNumeric(id))
				testRailResultUpdator.addResultToTestRail(testStatus, Integer.parseInt(id));
		}
	}

	/**
	 * This method is used to get the test case id
	 * @param strTestName - name of the test method 
	 * @return test case id if test method contains id, otherwise return null
	 */
	private String getCaseId(String strTestName){
		if(sys.getProperty("UpdateResultsToTestRail").equalsIgnoreCase("true")){
			String[] arr = strTestName.split("_");
			return arr[arr.length-1];
		}
		else
			return null;
	}

	@Override
	public void onFinish(ISuite arg0) {
		// TODO Auto-generated method stub
		
	}

}
